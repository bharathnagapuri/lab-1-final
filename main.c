#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#define NUM_THREADS 8
// Returns a random value between -1 and 1
double getRand(unsigned int *seed) {
    return (double) rand_r(seed) * 2 / (double) (RAND_MAX) - 1;
}

long double Calculate_Pi_Sequential(long long number_of_tosses) {
    // unsigned int seed = (unsigned int) time(NULL);
    double x=0;
    double sum = 0.0;
    double step = 1.0/(double) number_of_tosses;
    for (int i=0; i < number_of_tosses; i++) {
        x = (i+0.5)*step;
        sum = sum + 4.0/(1.0+ x*x);
    }
    sum = step * sum;

    return sum;
}

long double Calculate_Pi_Parallel(long long number_of_tosses) {

    
	 int i; 	  
	 double x, pi, sum[NUM_THREADS], step; 
	 step = 1.0/(double) number_of_tosses; 
	 omp_set_num_threads(NUM_THREADS); 
#pragma omp parallel 
	{	  
		double x;     int id; 
		id = omp_get_thread_num(); 
	    for (i=id, sum[id]=0.0;i< number_of_tosses; i=i+NUM_THREADS){ 
		    x = (i+0.5)*step; 
		    sum[id] += 4.0/(1.0+x*x); 
	    } 
	} 
	pi=0.0;
	for(i=0;i<NUM_THREADS;i++)
	pi += sum[i] * step; 

	return pi*(NUM_THREADS);


}

int main() {
    struct timeval start, end;

    long long num_tosses = 10000000;

    printf("Timing sequential...\n");
    gettimeofday(&start, NULL);
    long double sequential_pi = Calculate_Pi_Sequential(num_tosses);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    printf("Timing parallel...\n");
    gettimeofday(&start, NULL);
    long double parallel_pi = Calculate_Pi_Parallel(num_tosses);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    // This will print the result to 10 decimal places
    printf("π = %.10Lf (sequential)\n", sequential_pi);
    printf("π = %.10Lf (parallel)\n", parallel_pi);

    return 0;
}
